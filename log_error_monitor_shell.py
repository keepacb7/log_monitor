#! /bin/bash

#/opt/log/stash/ad-stream-etl/ad-stream-etl.log

for dir in {ad-stream-etl,ad-ext-tracking-dts}
do
result[0]=`grep "timed out" /opt/log/stash/$dir/$dir.log | tail -n 1 | awk -F \" {'print $4'}`   #再日志中过滤timed out 关键字，并把日志的时间放入result[0]中

if test -z "${result[0]}"    #如果没有检测到timed out 错误，表示为空
then
  echo "result is NULL"
else
  echo "$result is not Null"
  sleep 30
  result[1]=`grep "timed out" /opt/log/stash/$dir/$dir.log | tail -n 1 | awk -F \" {'print $4'}`
    if [ ${result[0]} == ${result[1]} ];then
       echo "${result[0]} == ${result[1]} the $dir error finished"     #等待30s后再取时间，如果result[0] 与 result[1] 结果相等则表示timed out 为最后一条，不再出现新的错误日志不再
    else
       curl 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=696e80ea-4dde-408d-b5a7-840f8c5be8d9' \
       -H 'Content-Type: application/json' -d '{"msgtype": "text","text": {"content": "'${result[1]}' '${dir}' connect timed out"}}'    #如果result[0] 与 result[1]，说明错误日志持续输出，报警
    fi
fi

echo ${result[0]}
echo ${result[1]}
done
